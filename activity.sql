-- To login to MySQL
mysql -u root -p

 -- For creating new database
 CREATE DATABASE blog_db;

 -- Creating tables in database
 CREATE TABLE Users (
        id INT NOT NULL AUTO_INCREMENT,
        email VARCHAR(50) NOT NULL,
        password VARCHAR(50) NOT NULL,
        Created Datetime NOT NULL,
        PRIMARY KEY(id)
    );

 CREATE TABLE Posts (
        id INT NOT NULL AUTO_INCREMENT,
        User_ID INT NOT NULL,
        Title VARCHAR(50) NOT NULL,
        Content VARCHAR(50) NOT NULL,
        Created Datetime NOT NULL,
        PRIMARY KEY(id),
        CONSTRAINT fk_posts_users_id
        FOREIGN KEY (User_ID) REFERENCES Users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
    );

 -- [SECTION] Creating new record/s for User.
 INSERT INTO Users (email, password, Created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
 INSERT INTO Users (email, password, Created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
 INSERT INTO Users (email, password, Created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
 INSERT INTO Users (email, password, Created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
 INSERT INTO Users (email, password, Created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

 -- [SECTION] Creating new record/s for Posts.
 INSERT INTO Posts (User_ID, Title, Content, Created) VALUES (1, "First Code", "Hello World!", "2021-01-01 01:00:00");
 INSERT INTO Posts (User_ID, Title, Content, Created) VALUES (2, "Second Code", "Hello Earth!", "2021-01-01 02:00:00");
 INSERT INTO Posts (User_ID, Title, Content, Created) VALUES (3, "Third Code", "Welcome to Mars!", "2021-01-01 03:00:00");
 INSERT INTO Posts (User_ID, Title, Content, Created) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-01 04:00:00");

 -- Get all post with an author id of 1

 SELECT User_ID, Title, Content, Created FROM Posts Where User_ID = 1;

 -- Get all the user's email and datetime of creation.

SELECT email, Created FROM Users;

-- -- Update a post's content to "Hello to the people of the Earth!” where its initial content is Hello Earth! by using the record s ID.

UPDATE Posts SET Content = "Hello to the people of the Earth!" WHERE id = 2;

-- Delete the user with an email of "johndoe@gmail.com".

DELETE FROM Users Where email = "johndoe@gmail.com";
